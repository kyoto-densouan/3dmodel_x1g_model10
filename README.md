# README #

1/3スケールのSHARP X-1G model10風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。

***

# 実機情報

## メーカ
- シャープ

## 発売時期
- 1986年7月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/X1_(%E3%82%B3%E3%83%B3%E3%83%94%E3%83%A5%E3%83%BC%E3%82%BF))
- [懐かしのパソコン](https://greendeepforest.com/?p=2401)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_x1g_model10/raw/d22980f940226169d5feb12543d8007ba14c3d5e/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x1g_model10/raw/d22980f940226169d5feb12543d8007ba14c3d5e/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_x1g_model10/raw/d22980f940226169d5feb12543d8007ba14c3d5e/ExampleImage.jpg)
